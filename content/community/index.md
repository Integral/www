# Community

Joining the FSFans community requires application. While we don't make it an requirement that you be a tech expert or use 100% free software, we do have a few requirements for  your membership application. Please read on to learn more.

## How to apply

To apply for membership, it is required that:

- You agree to follow our [Community Rules](rules.html);
- Your PGP key be signed by at least one of the existing members;
- You prove your contribution (code, translation, etc.) to the free software movement.

Please send your application e-mail to `goodspeed AT member DOT fsf DOT org`.

## Major channels

As a member, you can join the following channels in our community:

- Matrix: [Space](https://matrix.to/#/!RuAqctOicsivbVIXmB:halogen.city)
([Primary](https://matrix.to/#/!cCrSQKiIlmWmgMkzkD:frcsm.de),
[OT](https://matrix.to/#/!eHMfBBAPCVKrfXiBij:frcsm.de),
[AD](https://matrix.to/#/!SbisEgcIccIxJMuixf:halogen.city),
[WoT](https://matrix.to/#/!BTwiMVMRfTTaZzvaMx:frcsm.de));
- ~~XMPP: [fsfans-cn@chat.404.city](xmpp:fsfans-cn@chat.404.city)~~;
- ~~IRC: [\#fsfans-cn:libera.chat](ircs://irc.libera.chat:6697/#fsfans-cn) and [\#fsfans-cn:oftc.net](ircs://irc.oftc.net:6697/#fsfans-cn)~~.

XMPP and IRC are temporarily closed and will be reopened in the future.

## Elsewhere?

Unfortunately, you can't find FSFans anywhere else. All the official ways we communicate are listed above.

Members of FSFans might chat with each other on other platforms, but those platforms do not represent the official FSFans community.

Please note that we will especially NOT use QQ or WeChat which don't respect user freedom and privacy. A Telegram group is not considered; Matrix makes it unnecessary.

---

Copyright (C) 2022-2024 FSFans.club

This page is licensed under a [Creative Commons Attribution-ShareAlike 4.0 International License](http://creativecommons.org/licenses/by-sa/4.0/).
