# Community Rules

As a community of free software advocates, we strive to promote the notions of ethical technology and user freedom, and we wish to create a welcoming and inclusive community to achieve our goals. Therefore, we want for all our members to follow our do's and don'ts as below, which we believe make us healthy community:

1. DO respect others, regardless of their sexual orientation, gender identity, religious belief, political views, etc.
2. DO NOT promote proprietary software or any use of technology that subjugates the users, such as SaaSS, surveillance or censorship.
3. DO NOT talk about political ideas that are not related to free software.

Since the list is apparently incomplete, we urge that everyone in our community **use common sense** for any actions they are about to take. When we find it necessary, we will supplement our community rules.

---

Copyright (C) 2023-2024 FSFans.club

This page is licensed under a [Creative Commons Attribution-ShareAlike 4.0 International License](http://creativecommons.org/licenses/by-sa/4.0/).
