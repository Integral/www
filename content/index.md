---
{
	"nav": [
		{
			"link": "",
			"title": "HOME"
		},
		{
			"link": "/about",
			"title": "ABOUT"
		},
		{
			"link": "https://learn.fsfans.club",
			"title": "LEARN"
		},
		{
			"link": "/posts",
			"title": "POSTS"
		},
		{
			"link": "/services",
			"title": "SERVICES"
		},
		{
			"link": "/community",
			"title": "COMMUNITY"
		}
	]
}
---

<section>
	<div>
		<h1>Promoting Digital Freedom</h1>
		<p>
			Free Software Fans (FSFans) is a <a target="_blank" href="https://learn.fsfans.club/">Free Software</a> community.
			We promote software that respects users' freedom and privacy.
		</p>
	</div>
	<svg><use xlink:href="res/surveillance-camera.svg#nosurveillance"></svg>
</section>

<section>
	<div>
		<h1>Hacking for Fun</h1>
		<p>
			Making up FSFans are people who enjoy hacking GNU/Linux and other tech stuff.
		</p>
	</div>
	<svg><use xlink:href="res/laptop.svg#laptop"></svg>
</section>

<section>
	<div>
		<h1>Sharing for the Common Good</h1>
		<p>
			We at FSFans love sharing with each other
			what's really awesome, from an Emacs configuration to a chicken soup recipe.
		</p>
	</div>
	<svg><use xlink:href="res/conversation.svg#conversation"></svg>
</section>

<section>
	<div>
		<h1>Gaming Together</h1>
		<p>
			Feeling bored? How about playing some video games? FSFans has a number of game servers and a
			VoIP chat server.
		</p>
	</div>
	<svg><use xlink:href="res/game.svg#game"></svg>
</section>

<section class="join-us">
	<div>
		<h1>Wondering more or feeling like joining?</h1>
		<a href="/about/"><button>Learn More</button></a>
		<a href="/community/"><button>Join Us</button></a>
	</div>
</section>


---

Copyright (C) 2023 FSFans.club

This page is licensed under a [Creative Commons Attribution-ShareAlike 4.0 International License](http://creativecommons.org/licenses/by-sa/4.0/).
