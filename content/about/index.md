# About

Free Software Fans (a.k.a. FSFans) is a group of advocates devoted to promoting free software and its philosophy.

In general, the term "free software" means **software that you are free to use, modify and share**; and when speaking of `free', we mean freedom, not price.

Since software is deeply involved in all aspects of our lives, we are convinced that technologies that we make use of today (computer programs, smartphone apps, etc.) must be free enough to make sure that they're controlled by their users, not big companies. With the freedom that free software gives you comes other fundamental rights like freedom of speech and privacy.

As free software is becoming increasingly necessary in our lives, we felt obliged to team up with as many people as we could to remind people of the importance of digital freedom and teach them how to fight back for it. Founded in China, FSFans aims to fight for what you should have had in your digital life.

## Contact Us

- Founder, sysadmin: William Goodspeed <`goodspeed AT member DOT fsf DOT org`>;
- Webmaster: Peaksol <`p AT peaksol DOT org`>.

## Acknowledgements

Special thanks to SVGRepo for providing us with their free SVG icons published under CC0.

Our web design is greatly inspired by [gnu.org](https://gnu.org).

---

Copyright (C) 2022-2024 FSFans.club

This page is licensed under a [Creative Commons Attribution-ShareAlike 4.0 International License](http://creativecommons.org/licenses/by-sa/4.0/).
