# Rules of Our Services

While we do not yet have our formalized Terms of Service, we do require anyone, whether a member of us or not, that makes use of any of our services, to acknowledge and accept that:

1. You are not allowed to disrupt our ability to provide any of our services. This includes but is not limited to: attempting to cryptomine using our servers, spamming in our community chat rooms or forums, or attempting to distribute malware.
2. You may not violate any law that may be applicable to the jurisdiction where our service is provided. Some examples of unacceptable behavior in accordance to the law include, but are not limited to: harassment, scam attempts, and unauthorized copying and/or distribution of copyrighted content.
4. You are solely liable for any actions you take or contents you provide using any of our services. In no event shall the service provider take any liability arising from or in connection with your use of our service.
4. We provide our services on an "as is" and "as available" basis, and there is no guarantee that the service will be available at all times, meet expected performance, or fit a particular purpose. You are fully responsible for any damages in consequence of failure, defectiveness or insufficiency of our service.
6. We may change the rules of our services at any time in the future, with or without prior notice. By continuing to use any of our services, you agree to be bound by the latest rules of our services.

---

Copyright (C) 2023 FSFans.club

This page is licensed under a [Creative Commons Attribution-ShareAlike 4.0 International License](http://creativecommons.org/licenses/by-sa/4.0/).
