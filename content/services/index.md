# Services

We host a bunch of services on our own infrastructure, access to most of which may require [being a member of our community](../community/).

**By using any of our services, you agree to be bound by the [Rules of Our Services](rules.html).**

## Temporary suspension

As of now, most of our services are temporarily closed. We are looking to reopen them in the future.

## List of our services

- [Pubnix](ssh://tilde.fsfans.club/) - Our public-access \*nix system. *(Also comes with e-mail and web hosting, etc. Check our forum post for more deatils.)*
- [LDAP Tool Box](https://relay.fsfans.club/) - Control panel for managing your credential for our services.
- [Flarum](https://forum.fsfans.club) - A forum for our community.
- [The Lounge](https://webirc.fsfans.club/) - Modern, responsive, cross-platform web IRC client.
- [Matrix](https://fsfans.club) - An open network for secure, decentralized communication.
- [Mumble](mumble:voice.fsfans.club) - Low latency, high quality voice chat application. *(If you face high packet loss, try toggling `force TCP mode' in the settings of your client.)*
- [Xonotic](xonotic:gs.fsfans.club) - An addictive arena-style first person shooter.
- [PrivateBin](https://bin.fsfans.club) - A pastebin replacement where the server has zero knowledge of pasted data.
- [Forgejo](https://src.fsfans.club/) - A lightweight code hosting and code collaboration solution.

## What infrastructure are our services on?

We have four servers running in different locations:

1. **relay.fsfans.club** (New York, United States)
2. **fsfans.club** (New York, United States)
3. **sh.fsfans.club** (Shanghai, China)
4. **tilde.fsfans.club** (Hebei, China)

---

Copyright (C) 2023-2024 FSFans.club

This page is licensed under a [Creative Commons Attribution-ShareAlike 4.0 International License](http://creativecommons.org/licenses/by-sa/4.0/).
