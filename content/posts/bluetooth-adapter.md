# 仅 $1 的 USB 蓝牙适配器测评

前几天电脑上的有线耳机被我扯断了，所以我打算用蓝牙连无线耳机。电脑上没有板载蓝牙，所以找找有什么蓝牙适配器。

蓝牙这东西好像没有无线网卡那么邪乎，需要专有固件的比较少。所以我在淘宝上随便挑了个最便宜的。

- 价格：￥6.8 包邮
- 型号：蓝牙 5.0 迷你型【免驱升级版】简约版

## 硬件信息

```
0a12:0001 Cambridge Silicon Radio, Ltd Bluetooth Dongle (HCI mode)
```

对应 h-node 上的信息：<https://h-node.org/bluetooth/view/en/167>

## 使用方法

我使用的系统是 Parabola GNU/Linux-libre，没有专有固件。

在安装 `bluez` 和开启 `bluetoothd` 服务后就能直接进行蓝牙操作，无需额外驱动配置。
因为是用来连接蓝牙耳机，还需要装上 `pulseaudio` 和 `pulseaudio-bluez`。

连接也很简单，只需要在终端打开 `bluetoothctl`：

```
# scan on
```

你能看到很多设备被扫描出来，找到你的蓝牙耳机的 MAC 地址，然后连接即可：

```
# pair XX:XX:XX:XX:XX:XX
# trust XX:XX:XX:XX:XX:XX
# connect XX:XX:XX:XX:XX:XX
```

这时可能还没有声音，在 pavucontrol 里简单设置一下就行。

---

Copyright (C) 2023 William Goodspeed

This page is licensed under a [Creative Commons Attribution-ShareAlike 4.0 International License](http://creativecommons.org/licenses/by-sa/4.0/).
