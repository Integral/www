# 自由无线网卡选购指南

> 许多的无线网卡都需要非自由固件，这些网卡在搭载 Linux-libre 的操作系统（例如 Trisquel GNU/Linux）上没法运行。这里就为大家推荐几款可以在自由固件下使用的网卡。文章大部分内容从互联网中收集，部分设备我没有测试过。我不对真实性负责，但文章里的内容应该大都正确。

首先，第一原则：不要听信淘宝商家说的免驱，这不代表在自由的 GNU/Linux 操作系统上免驱。而且，往往是那些不免驱的网卡可以在 GNU/Linux 很好地工作。

## 150 兆 Lite-N USB 网卡

TPLINK TL-WN721N 网卡是中国市场保有量较多的网卡，咸鱼上售价二十到四十不等。选购时请尤其注意，选择版本是 1.0、2.0、3.0、3.1 的网卡。你可以要求商家提供网卡背面的截图，如果上面写着 WN721N Ver 1.0 2.0之类的那就没有问题了。

TPLINK TL-WN721N 网卡还有个兄弟，那就是 TL-WN721NC。大小比 TL-WN721N 小一些，比较适合插在笔记本电脑上。

两个网卡都使用 Atheros AR9002U 芯片和 Atheros AR9271 芯片。无线制式为 b/g/n，150 兆的速度可以基本上满足上网需求。

Trisquel 与其它大多数 Debian/Ubuntu 发行版上安装 open-ath9k-htc-firmware 包就能驱动 WN721N。如果使用的是其它发行版也可以手动编译 ath9k-htc 固件，源代码位于 Github。

## 150 兆高增益 USB 网卡

TPLINK TL-WN722N 网卡与上面的 TL-WN721 网卡的天线不同。TL-WN722N 网卡总共有五大版，而我只搜集到了前三版的资料。

- 1.x 版本的 WN722N 使用的是 Atheros AR9002U 与 Atheros AR9271 芯片，1.0 与 1.1 版只有 PCB 差异。；
- 2.x 版本的 WN722N 使用的是 Realtek RTL8188EUS 芯片，需要专有固件驱动；
- 3.x 是 2.x 版本的升级版，使用同样的芯片，同样需要专有固件。

和 TL-WN721N 一样，它也有个弟弟——TL-WN722NC。NC 版本比 N 版多了个底座，国内不好买到完整的 NC 版本。很多版本可以外接天线，接口类型为 RP-SMA，制式依旧为 b/g/n。售价相较 TL-WN721N 贵些。

驱动安装方法与 TL-WN721N 一致。

## 54 兆 USB 无线网卡

比起上两个，TPLINK TL-WN322G 要比他们早生产。所以……速度也很感人，但还是够用的。

购买时要注意版本，1.x 版本使用的芯片是 ZyDAS ZD1211B，需要的固件有二进制 blob，3.x 版本使用的是和上面相同的 Atheros AR9002U/AR9271 芯片。

驱动安装方法与TL-WN721N一致。

## 300/450 兆单/双 PCIe 无线网卡

杀手 killer n1102 无线网卡有着不同的配置版本，分别是单双频和三百四百五兆版。

这些版本配置不同搭载的芯片也不同，但都由 Atheros 生产。它们都可以使用自由固件——ath9k驱动。

这里顺便附上淘宝的宝贝链接：<https://item.taobao.com/item.htm?id=36203679539>

无线制式为 a/b/g/n，驱动安装方法与 TL-WN721N 一致。

## 150/300 兆 mPCIe/M.2 无线网卡

Atheros 的 AR9565 / QCA9565 芯片，可以在 Trisquel 10 及以上版本即插即用。

两者的常见名称分别为 QCWB335 和 QCNFA335，其中后者也可以通过 DW1707（Dell Wireless 1707）商品名搜到。

两款网卡都能在 10 元左右的价格买到，购买时注意看接口类型，前者为 mini-PCIe，后者为 M.2（NGFF）。

文章的最后，希望大家都可以选购到自己喜欢的自由无线网卡！

---

Copyright (C) 2021 William Goodspeed  
Copyright (C) 2023 Peaksol

This page is licensed under a [Creative Commons Attribution-ShareAlike 4.0 International License](http://creativecommons.org/licenses/by-sa/4.0/).
