# FSFans 2024 LAN Party Beijing

![现场照片](IMG_20240120_205449.jpg)

- 地点：北京林业大学学研中心
- 时间：2024-01-20 10:00 CST

---

Copyright (C) 2024 FSFans.club

This page is licensed under a [Creative Commons Attribution-ShareAlike 4.0 International License](http://creativecommons.org/licenses/by-sa/4.0/).
