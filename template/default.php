<?php
global $root_frontmatter;
$is_root_index = ($dir_relative == '' && $basename == 'index.md');
if($is_root_index) {
	$root_frontmatter = $frontmatter;
}

$hrpos = strrpos($parsed, '<hr />');
$page_footer = '';
if($hrpos !== false) {
	$page_footer = substr($parsed, $hrpos + 6);
	$parsed = substr($parsed, 0, $hrpos);
}
?>
<!DOCTYPE html>
<html>
<head>
	<title><?php if(!$is_root_index) echo $frontmatter['title'] . ' - '; ?>Free Software Fans</title>

	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1">

	<link href="/css/main.css" rel="stylesheet">
</head>

<body>
	<div id="top">
		<a class="skip" href="#content"><b>Skip to main text</b></a>
	</div>

	<div id="header">
		<p id="banner">
			<svg width="64px" height="64px"><use xlink:href="/res/fsfans-logo.svg#logo"></svg>
			<svg width="360px" height="64px" aria-label="Free Software Fans"><use xlink:href="/res/fsfans-arttype.svg#fsfans"></svg><br />
			<small>A free software community in China</small>
		</p>

		<ul id="nav">
			<li><a href="#nav"></a><a href="#content"></a></li>
			<?php foreach($root_frontmatter['nav'] as $item): ?>
				<li><a href="<?= $item['link'] . '/' ?>" <?php if($item['link'] == $dir_relative) echo 'class="dim"'; ?>><?= $item['title'] ?></a></li>
			<?php endforeach; ?>
		</ul>
	</div>

	<div id="content" <?php if($is_root_index) echo 'class="frontpage"'; ?>>
		<?= $parsed ?>
	</div>

	<footer>
		<div class="back-to-top"><a href="#top">▲</a></div>
		<?= $page_footer ?>
		<p>Free Software Fans (FSFans) is not affiliated with the Free Software Foundation or the GNU Project.</p>
	</footer>
</body>
</html>
