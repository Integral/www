// @license magnet:?xt=urn:btih:90dc5c0be029de84e523b9b3922520e79e0e6f08&dn=cc0.txt CC0-1.0

// Configure a theme dynamically
// Put this script to <head> to prevent repainting

// toggle
let linkcss = null;
function theme() {
    if (linkcss === null) {
        linkcss = document.createElement('link');
        Object.assign(linkcss, {
            rel: 'stylesheet',
            href: 'fortune/fortune.css'
        });
        document.head.appendChild(linkcss);
        localStorage.setItem('custom_theme', '1');
    } else {
        linkcss.remove();
        linkcss = null;
        localStorage.setItem('custom_theme', '');
    }
}

// bind toggle to a button
document.addEventListener('DOMContentLoaded', function() {
    const toggle = document.querySelector('a[href="#theme"]');
    if (toggle !== null) toggle.addEventListener('click', function() {
        // transition for eyes
        const style = document.createElement('style');
        document.head.appendChild(style);
        style.sheet.insertRule(`* {
            transition-delay: 0.6s, 0s, 0s;
            transition-duration: 1.5s, 0.6s, 0.6s;
            transition-property: background-color, color, border;
            transition-timing-function: ease-in, ease-out, step-start;
        }`, 0);
        theme();
    });
});

// persist preference
if (localStorage.getItem('custom_theme')) theme();

// @license-end
