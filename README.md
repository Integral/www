# Notes for Webmasters

This site uses `peaksol-org-ssg` as its static site generator. User guide is available [here](https://codeberg.org/Peaksol/peaksol-org-ssg).

Here is a quick reference for you to perform common operations to the site.

## Creating a page

Create a `.md` file in `contents/` and write whatever you want.

To use a custom title rather than the one automatically generated, start the file with:

```
---
{"title": "Lorem Ipsum"}
---
```

To add a copyright notice, end the file with:

```md
---

Copyright (C) [YEAR] [YOUR NAME]

This page is licensed under a [Creative Commons Attribution-ShareAlike 4.0 International License](http://creativecommons.org/licenses/by-sa/4.0/).
```

## Editing navbar items

If you want a new directory to show up on the navbar, edit the JSON in `content/index.md`.

## Static files

Static files can be placed in `content/`. If they are especially for web styling rather than the content, place them in `template/assets/`.

## Generating the site

Edit `generate.sh` to choose the target directory. After that:

```sh
./generate.sh
```

**Please note that running the script will remove all files in the out directory.** Make sure you set the path correctly before you run the script. If you don't want to clean the directory before generation, just remove the `--clean` option.
